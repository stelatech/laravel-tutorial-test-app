<?php

// Route to the default homepage - "index". 
// This calles the home() method inside HomeController.
Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@home'
));

Route::get('/user/{username}', array(
    'as' => 'profile-user',
    'uses' => 'ProfileController@user'
));

// Authenticated group
Route::group(array('before' => 'auth'), function(){
    
    // CSRF protection - prevent session hijacking
    Route::group(array('before' => 'csrf'), function(){
        
        Route::post('account/change-password', array(
            'as' => 'account-change-password',
            'uses' => 'AccountController@postChangePassword'
        ));
        
    });
    
    // Sign Out (GET)
    Route::get('account/sign-out', array(
        'as' => 'account-sign-out',
        'uses' => 'AccountController@getSignOut'
    ));
    
    Route::get('account/change-password', array(
        'as' => 'account-change-password',
        'uses' => 'AccountController@getChangePassword'
    ));
    
});

// Unauthenticated group - a route group. The array allows us to define 
// what to do with a filter specified in filters.php. In this case, we are
// defining routes that allow unlogged users to access certain pages
Route::group(array('before' => 'guest'), function() {

    // CSRF protection - prevent session hijacking
    Route::group(array('before' => 'csrf'), function() {
        
        // Create account (GET)
        Route::post('/account/create', array(
            'as' => 'account-create-post',
            'uses' => 'AccountController@postCreate'
        ));
        
        // Sign In (POST)
        Route::post('/account/sign-in', array(
           'as' => 'account-sign-in',
           'uses' => 'AccountController@postSignIn'
        ));
        
        // Forgot Password (POST)
        Route::post('/account/forgot-password', array(
            'as' => 'account-forgot-password',
            'uses' => 'AccountController@postForgotPassword'
        ));
        
    });

    // Sign In (GET)
    Route::get('/account/sign-in', array(
       'as' => 'account-sign-in',
       'uses' => 'AccountController@getSignIn'
    ));
    
    // Create account (GET)
    Route::get('/account/create', array(
        'as' => 'account-create',
        'uses' => 'AccountController@getCreate'
    ));

    // Activate account (GET)
    Route::get('/account/activate/{code}', array(
        'as' => 'account-activate',
        'uses' => 'AccountController@getActivate'
    ));
    
    // Forgot password (GET)
    Route::get('/account/forgot-password', array(
        'as' => 'account-forgot-password',
        'uses' => 'AccountController@getForgotPassword'
    ));
    
    Route::get('/account/recover/{code}', array(
        'as' => 'account-recover',
        'uses' => 'AccountController@getRecover'
    ));
    
    /*
     * The below code is a REST-ful way of doing routes - controller is automatically
     * binded to a subdirectory. However, to make it easier for other
     * developers to understand/handle our routes, as well as group or customize
     * alot of things, even though it requires alot more writing of code,
     * writing routes individually is the way to go.
      Route::controller('account', 'AccountController');
     */
});
