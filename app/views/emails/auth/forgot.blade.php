Hello {{ $username }},

It looks like you requested a new password. You'll need to use the following link to activate your password

New password: {{ $password }}<br/><br/>
------<br/>
{{ $link }}<br/>
-------