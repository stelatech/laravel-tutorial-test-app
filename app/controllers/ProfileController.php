<?php
class ProfileController extends BaseController{
    public function user($username){
        $user = User::where('username', '=', $username);
        
        if($user->count()){
            $user = $user->first();
            return View::make('profile.user')->with('user', $user);
        }
        
        // No user - 404 page
        return App::abort(404);
    }
}