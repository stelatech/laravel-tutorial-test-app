## Laravel Test App - PHPAcademy

This Test Application was built using the Laravel PHP Framework. 
I followed the PHPAcademy tutorial found here - 
[https://www.youtube.com/playlist?list=PLfdtiltiRHWGf_XXdKn60f8h9jjn_9QDp]
(https://www.youtube.com/playlist?list=PLfdtiltiRHWGf_XXdKn60f8h9jjn_9QDp).

### Laravel Information

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

Official Repository for the Laravel Framework [laravel/framework](http://github.com/laravel/framework).

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
